package com.example.apprecetas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText txt_nombre;
    private TextView txt_area_ingre,txt_area_nutri,txt_title_receta;
    private Button bt_buscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txt_area_ingre = (TextView) findViewById(R.id.txt_area_ingre);
        this.txt_area_nutri = (TextView) findViewById(R.id.txt_area_nutri);
        this.txt_nombre = (EditText) findViewById(R.id.txt_nombre);
        this.bt_buscar = (Button) findViewById(R.id.bt_buscar);
        this.txt_title_receta = (TextView)findViewById(R.id.txt_title_receta) ;

        bt_buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                String nombre = txt_nombre.getText().toString().trim();



                String url = "https://test-es.edamam.com/search?q="+nombre+"&app_id=70c965f4&app_key=7651610019c11ca39a487631f59940fe";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);


                                    JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject primer = hitsJSON.getJSONObject(0);
                                    JSONObject recipesJSON  = primer.getJSONObject("recipe");
                                    String titulo = recipesJSON.getString("label");
                                    txt_title_receta.setText(titulo);
                                    JSONArray ingreARRAY = recipesJSON.getJSONArray("ingredientLines");
                                    String lista = "" ;

                                    for(int i = 0; i<ingreARRAY.length();i++){

                                        lista = lista+ ingreARRAY.getString(i)+ "\n";


                                    }
                                    txt_area_ingre.setText(lista);


                                    JSONObject nutJSON = recipesJSON.getJSONObject("totalNutrients");
                                    JSONObject enerJSON =nutJSON.getJSONObject("ENERC_KCAL");
                                    String energiaetiqueta = enerJSON.getString("label");
                                    String energia = enerJSON.getString("quantity");
                                    String botd = enerJSON.getString("unit");

                                    JSONObject fatJSON =nutJSON.getJSONObject("FAT");
                                    String fafc =fatJSON.getString("label");
                                    String motod = fatJSON.getString("quantity");
                                    String futuro = fatJSON.getString("unit");

                                    JSONObject fasatJSON =nutJSON.getJSONObject("FASAT");
                                    String lector = fasatJSON.getString("label");
                                    String face = fasatJSON.getString("quantity");
                                    String nitro = fasatJSON.getString("unit");


                                    String todo = energiaetiqueta + " - " + energia + " - " + botd + "\n" +
                                            fafc + " - " + motod + " - " + futuro + "\n" +
                                            lector + " - " + face + " - " + nitro + "\n" ;

                                    txt_area_nutri.setText(todo);

                                    txt_nombre.setText("");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);


            }
        });
    }
}
